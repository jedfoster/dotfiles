#!/usr/bin/env bash

# Init and checkout all the submodules
cd ~/Code/dotfiles && git submodule init && git submodule update --checkout && cd -;

# Symlink files to their traditional location in $HOME
ln -s ~/Code/dotfiles/zshrc  ~/.zshrc;
touch ~/Code/dotfiles/zsh_history
ln -s ~/Code/dotfiles/zsh_history  ~/.zsh_history;
ln -s ~/Code/dotfiles/git/gitconfig  ~/.gitconfig;
ln -s ~/Code/dotfiles/git/gitignore_global  ~/.gitignore_global;
ln -s ~/Code/dotfiles/scripts/git-completion.bash  ~/.git-completion.bash;
ln -s ~/Code/dotfiles/vim  ~/.vim;
ln -s ~/Code/dotfiles/vim/gvimrc  ~/.gvimrc;
ln -s ~/Code/dotfiles/vim/vimrc  ~/.vimrc;
ln -s ~/Code/dotfiles/vim/vimrc_background  ~/.vimrc_background;
ln -s ~/Code/dotfiles/ctags ~/.ctags
ln -s ~/Code/dotfiles/prettierrc.json ~/.prettierrc.json

# Install nvm
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.35.3/install.sh | bash

# Install additional dependencies
brew install --cask iterm2
brew install --cask macvim
brew install the_silver_searcher
brew install vcprompt
brew tap universal-ctags/universal-ctags
brew install --HEAD universal-ctags
brew install defaultbrowser
brew install jq
brew install prettier

# Install various Quick Look plugins, from github.com/sindresorhus/quick-look-plugins
brew install qlcolorcode qlstephen qlmarkdown quicklook-json qlimagesize suspicious-package quicklookase qlvideo

# Install Prettier inside vim-prettier
cd vim/bundle/vim-prettier && npm install && cd -

# Set screen shot directory and turn off drop shadows
mkdir ~/Library/Mobile\ Documents/com~apple~CloudDocs/Screen\ Shots
defaults write com.apple.screencapture location ~/Library/Mobile\ Documents/com~apple~CloudDocs/Screen\ Shots
defaults write com.apple.screencapture disable-shadow -bool TRUE
killall SystemUIServer

# Install my preferred fonts
cp -n ~/Code/dotfiles/fonts/* ~/Library/Fonts/
