# https://scriptingosx.com/2019/06/moving-to-zsh-part-3-shell-options/

HISTFILE=${ZDOTDIR:-$HOME}/.zsh_history
SAVEHIST=10000000
HISTSIZE=2000
HISTTIMEFORMAT="%Y-%M-%d %T  "

setopt EXTENDED_HISTORY

# share history across multiple zsh sessions
# This causes leakage between sessions when scrolling up through previous
# commands. Commands from other sessions show up, when all I want is commands
# from the _CURRENT_ session.
# setopt SHARE_HISTORY

# append to history
setopt APPEND_HISTORY

# adds commands as they are typed, not at shell exit
# This does cause commands from all sessions to be interleaved, since they are
# written AS they execute. Makes it a bit more muddled than bash.
setopt INC_APPEND_HISTORY

# expire duplicates first
setopt HIST_EXPIRE_DUPS_FIRST
# do not store duplications
setopt HIST_IGNORE_DUPS
#ignore duplicates when searching
setopt HIST_FIND_NO_DUPS
# removes blank lines from history
setopt HIST_REDUCE_BLANKS
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
# (some of these are redundant)

# show the substituted commands (!!, !$, etc.) in the prompt, giving you a
# chance to edit or cancel it, or just confirm it.
setopt HIST_VERIFY

setopt AUTO_CD

setopt CORRECT
# setopt CORRECT_ALL

if [ -d "$HOME/Code/dotfiles/scripts" ]; then
  PATH="$HOME/Code/dotfiles/scripts:$PATH"
fi

eval "$(rbenv init - zsh)"

. ~/Code/dotfiles/zsh/private
